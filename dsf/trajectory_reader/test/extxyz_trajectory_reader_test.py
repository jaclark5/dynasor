import numpy as np
import unittest
from dsf.trajectory_reader.extxyz_trajectory_reader \
    import extxyz_trajectory_reader as trajectory_reader
from dsf.trajectory_reader.test.trajectory_reader_test_mixin \
    import TrajectoryReaderTestMixin





class EXTXYZTrajectoryReaderTest(unittest.TestCase):

    filename_xyz = 'data/dump.xyz'
    filename_xyz_with_velocities = 'data/dump_with_velocities.xyz'

    def test_open_no_velocities(self):
        trajectory_reader(self.filename_xyz)

    def test_open_with_velocities(self):
        trajectory_reader(self.filename_xyz_with_velocities)

    def test_read_frames(self):
        # dump with position
        reader = trajectory_reader(self.filename_xyz)
        frames = list(reader)
        self.assertEqual(len(frames), 4)
        for it, frame in enumerate(frames, start=1):
            self.assertEqual(it, frame['index'])
            self.assertIn('x', frame.keys())
            self.assertEqual(frame['x'].shape, (3, 1080))


        # dump with position and velocities
        reader = trajectory_reader(self.filename_xyz)
        frames = list(reader)
        self.assertEqual(len(frames), 4)
        for it, frame in enumerate(frames, start=1):
            self.assertEqual(it, frame['index'])
            self.assertIn('x', frame.keys())
            self.assertIn('v', frame.keys())
            self.assertEqual(frame['x'].shape, (3, 1080))            
            self.assertEqual(frame['v'].shape, (3, 1080))            


    def test_first_frame_contents(self):
        reader = trajectory_reader(self.filename_xyz)
        frame = next(reader)

        self.assertEqual(frame['N'], 1080)
        self.assertEqual(frame['index'], 1)

        # first five poisitions        
        x_target5 = 0.1 * np.array([[35.49864674, 35.46639586, 0.17918569], 
                                    [3.01637108, 3.02819058, 2.89811028],
                                    [3.33355666, 3.05802449, 0.00510028],
                                    [2.38218237, -0.02001685, 2.92525985],
                                    [35.51266599, 3.65396702, 2.90811710]]).T

        x_read5 = frame['x'][:, 0:5]
        self.assertTrue(np.allclose(x_read5, x_target5))

    def test_first_frame_contents_with_velocities(self):
        reader = trajectory_reader(self.filename_xyz_with_velocities)
        frame = next(reader)
        self.assertEqual(frame['N'], 1080)
        self.assertEqual(frame['index'], 1)

        # first five velocities
        v_target5 = 0.1 * np.array([[0.00110336, 0.00111412, 0.00155735],
                                    [-0.00100465, 0.00085346, -0.00125993],
                                    [0.00139634, 0.00269955, -0.00236018],
                                    [0.00059981, -0.00173836, -0.00372708],
                                    [0.00247676, 0.00041072, -0.00134462]]).T

        v_read5 = frame['v'][:, 0:5]
        self.assertTrue(np.allclose(v_read5, v_target5))

