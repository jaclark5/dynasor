%
% Plot script for current correlations
% Takes a few seconds to load and plot

clear all
close all
clc

% Colors
red=[0.9 0.3 0.3];
blue=[0.3 0.3 0.9];
black=[0.1 0.1 0.1];
green=[0.3 0.9 0.3];
purple=[0.8 0.4 0.8];
% Colors

fontsize=20;
fontstyle='Times';

% Conversion factors
hbar=1.05457173*1e-34; % m2 kg / s
J2ev=6.24150934e18;

a=4.065/10; %nm
T=300;


%% C(k,t) and C(k,w) for two k-values

eval(strcat('dynasor_outT',num2str(T),'_GK_old'))
w=hbar*w*J2ev*1000*1e15; %meV
k=k*a/(2*pi);

kind1=34;
kind2=68;
LW=4.0;
wmax=40;
[tmp,wInd]=min(abs(w-wmax));
xlims1=[0 3000];
xlims2=[0 w(wInd)];

figure('Position',[100 100,1600,900],'Color','w')
s=suptitle('Gamma to K T=300');

subplot(2,2,1)
plot(t,Cl_k_t_0_0(:,kind1),'Color',red,'LineWidth',LW)
hold on
plot(t,Ct_k_t_0_0(:,kind1),'Color',blue,'LineWidth',LW)
ylabel('C(k,t)')
xlabel('Time [fs]')
h=legend(strcat('Longitudinal , k=',num2str(k(kind1))),strcat('Transverse k=',num2str(k(kind1))));
set(h,'Edgecolor','w');
xlim(xlims1)

subplot(2,2,2)
plot(t,Cl_k_t_0_0(:,kind2),'Color',red,'LineWidth',LW)
hold on
plot(t,Ct_k_t_0_0(:,kind2),'Color',blue,'LineWidth',LW)
xlabel('Time [fs]')
h=legend(strcat('Longitudinal , k=',num2str(k(kind2))),strcat('Transverse k=',num2str(k(kind2))));
set(h,'Edgecolor','w');
xlim(xlims1)

subplot(2,2,3)
plot(w,Cl_k_w_0_0(:,kind1),'Color',red,'LineWidth',LW)
hold on
plot(w,Ct_k_w_0_0(:,kind1),'Color',blue,'LineWidth',LW)
xlabel('\omega [meV]')
ylabel('C(k,\omega)')
h=legend(strcat('Longitudinal , k=',num2str(k(kind1))),strcat('Transverse k=',num2str(k(kind1))));
set(h,'Edgecolor','w');
xlim(xlims2)

subplot(2,2,4)
plot(w,Cl_k_w_0_0(:,kind2),'Color',red,'LineWidth',LW)
hold on
plot(w,Ct_k_w_0_0(:,kind2),'Color',blue,'LineWidth',LW)
xlabel('\omega [meV]')
h=legend(strcat('Longitudinal , k=',num2str(k(kind2))),strcat('Transverse k=',num2str(k(kind2))));
set(h,'Edgecolor','w');
xlim(xlims2)

set(findall(gcf,'-property','FontSize'),'FontSize',fontsize)
set(findall(gcf,'-property','FontName'),'FontName',fontstyle)

%% Smoothed current map of both Gamma to L and Gamma to K.

wmax=50;
[tmp,wInd]=min(abs(w-wmax));
ylims=[0 w(wInd)];
xlims1=[k(1) k(end)];

eval(strcat('dynasor_outT',num2str(T),'_GK_old'))
w=hbar*w*J2ev*1000*1e15; %meV
k=k*a/(2*pi);
smooth_CL_GK=zeros(wInd,length(k));
smooth_CT_GK=zeros(wInd,length(k));
for i=1:length(k)
    smooth_CL_GK(:,i)=smooth(Cl_k_w_0_0(1:wInd,i),10,'lowess');
    smooth_CT_GK(:,i)=smooth(Ct_k_w_0_0(1:wInd,i),10,'lowess');
end
k_GK=k;


eval(strcat('dynasor_outT',num2str(T),'_GL_old'))
w=hbar*w*J2ev*1000*1e15; %meV
k=k*a/(2*pi);
smooth_CL_GL=zeros(wInd,length(k));
smooth_CT_GL=zeros(wInd,length(k));
for i=1:length(k)
    smooth_CL_GL(:,i)=smooth(Cl_k_w_0_0(1:wInd,i),10,'lowess');
    smooth_CT_GL(:,i)=smooth(Ct_k_w_0_0(1:wInd,i),10,'lowess');
end

Full_current=[fliplr(smooth_CL_GL) smooth_CL_GK] + [fliplr(smooth_CT_GL) smooth_CT_GK];
k_GL=-fliplr(k);
K=[k_GL k_GK];


figure('Position',[100 100,1300,700],'Color','w')
surf(K,w(1:wInd),Full_current,'EdgeColor','none')

ylabel('\omega [meV]')
title(strcat('Sum of both currents T=',num2str(T)))
view([0 90])
ylim(ylims)
xlim([K(1) K(end)])
pos = get(gca, 'Position');
pos(1) = 0.055;
pos(2) = 0.07;
pos(3) = 0.9;
pos(4) = 0.85;
set(gca, 'Position', pos)
set(gca,'XTick',[K(1) 0 K(end)],'XTickLabel',{'L','Gamma','K'})

set(findall(gcf,'-property','FontSize'),'FontSize',fontsize)
set(findall(gcf,'-property','FontName'),'FontName',fontstyle)
